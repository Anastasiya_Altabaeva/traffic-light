package com.example.trafficlight;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private ConstraintLayout myConstraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myConstraintLayout = findViewById(R.id.constraintLayout);

    }

    public void onClickRedButton(View view){
        myConstraintLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.colorRed));
    }

    public void onClickYellowButton(View view){
        myConstraintLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.colorYellow));
    }

    public void onClickGreenButton(View view){
        myConstraintLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.colorGreen));
    }
}
